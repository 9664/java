public class Ecuacion2grado {
    public static void main(String []args) {
        double a=Double.parseDouble(args[0]);
        double b=Double.parseDouble(args[1]);
        double c=Double.parseDouble(args[2]);
        double x1, x2, d;

        x1= (-b + Math.sqrt((b*b)-(4*a*c)))/(2*a);
        x2= (-b - Math.sqrt((b*b)-(4*a*c)))/(2*a);

       System.out.println("Los resultados son: "+x1+" y " +x2+".");
    }
}
