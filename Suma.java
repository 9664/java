import java.util.Scanner;

public class Suma
{
	
	public static void main(String [] args)
	{
		Scanner reader = new Scanner (System.in);
		System.out.println("Introduce el primer número:");
		double n1 = reader.nextDouble();

		System.out.println("Introduce el segundo número:");
		double n2 = reader.nextDouble();

		double resultado = n1 + n2;
			
		System.out.println("LA SUMA ES: " + resultado);
	}
}
