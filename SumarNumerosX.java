import java.util.Scanner;

class SumarNumerosX {
    public static void main(String args[])
    {
        Scanner sc=new Scanner(System.in);
        int n,counter,suma;
        System.out.println("Introduce un número: ");
        n=sc.nextInt();    // 5

        counter = 1;
        suma = 0;

        while(counter <= n){ //counter vale 1, 2, 3, 4, 5, 6
            suma = suma + counter;  // suma vale 1, 3, 6, 10, 15
            
            counter++;  //counter=counter+1

       }
    System.out.println("El resultado es:" +suma);
    }
}
