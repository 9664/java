
public class MayorMenor {
	public static void main(String args[]) {
		//Scanner s=new Scanner(System.in);
		int t, horas, minutos, segundos, segundosSobra;
		t=Integer.parseInt(args[0]);
		
		//t=s.nextInt();
		horas=t/3600;
		segundosSobra=t%3600;
		minutos=segundosSobra/60;
		segundos=segundosSobra%60;
		System.out.println(t+" segundos equivalen a: "+horas+" horas, "+minutos+" minutos, "+segundos+" segundos.");
	}
}
